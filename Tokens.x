{ 
module Tokens where 
import Data.List
import Data.Char (isSpace)
}

%wrapper "posn" 
$digit = 0-9     
-- digits 
$alpha = [a-zA-Z]    
-- alphabetic characters

@http = "http://" | "https://"
@hostpart = [$alpha $digit \_ \-]+
@pathpart = [$alpha $digit \- \_ \~ \: \% \/ \? \# \[ \] \@ \! \$ \& \' \( \) \* \+ \, \; \= \.]+

@all = [$alpha $digit \" \£ \% \^ \{ \} \< \> \\ \| \` \¬ \| \- \_ \~ \: \/ \? \[ \] \@ \! \$ \& \' \( \) \* \+ \, \; \= \. $white]

tokenpairs :-
  $white+                                                           ;--{ \p s -> (p, TokenWhite) }
  "#" @all* "#"                                                     ;
  "from"                                                            { \p s -> (p, TokenFrom) }
  "file"                                                            { \p s -> (p, TokenFile) }
  "where"                                                           { \p s -> (p, TokenWhere) }
  "output"                                                          { \p s -> (p, TokenOutput) }
  "true"                                                            { \p s -> (p, TokenBool True) }
  "false"                                                           { \p s -> (p, TokenBool False) }
  "replacement"                                                     { \p s -> (p, TokenReplacement) }
  "of"                                                              { \p s -> (p, TokenOf) }
  "NULL"                                                            { \p s -> (p, TokenNull) }
  "ANY"                                                             { \p s -> (p, TokenAny) }
  ","                                                               { \p s -> (p, TokenComma) }
  "."                                                               { \p s -> (p, TokenDot) }
  "+"                                                               { \p s -> (p, TokenPlus) }
  "-"                                                               { \p s -> (p, TokenMinus) }
  "*"                                                               { \p s -> (p, TokenTimes) }
  "/"                                                               { \p s -> (p, TokenDiv) }
  ";"                                                               { \p s -> (p, TokenEnd) }
  "object" "s"?                                                     { \p s -> (p, TokenObject) }
  "subject" "s"?                                                    { \p s -> (p, TokenSubject) }
  ">"                                                               { \p s -> (p, TokenGreaterThan) }
  "<"                                                               { \p s -> (p, TokenLessThan) }
  "predicate" "s"?                                                  { \p s -> (p, TokenPredicate)}
  "{"                                                               { \p s -> (p, TokenLBracket) }
  "}"                                                               { \p s -> (p, TokenRBracket) }
  "and"                                                             { \p s -> (p, TokenAnd) }
  "application"                                                     { \p s -> (p, TokenApplication) }
  "isn't"                                                           { \p s -> (p, TokenIsnt) }
  "is"                                                              { \p s -> (p, TokenIs) }
  "to"                                                              { \p s -> (p, TokenTo) }
  "in"                                                              { \p s -> (p, TokenIn) }
  "load"                                                            { \p s -> (p, TokenLoad) }
  "x"                                                               { \p s -> (p, TokenX) }
  "-"? $digit+                                                      { \p s -> (p, TokenInt (read $ trim s)) }
  \" $alpha* \"                                                     { \p s -> (p, TokenString (read $ tail $ init $ trim s)) }
  $alpha [$alpha $digit \_]* \. [$alpha $digit]*                    { \p s -> (p, TokenFileName (trim s)) }
  $alpha [$alpha $digit \_]* "'s"                                   { \p s -> (p, TokenPossesive (init $ init $ trim s)) }
  $alpha [$alpha $digit \_]*                                        { \p s -> (p, TokenVar (trim s)) }
  "<" @http @hostpart (\. @hostpart)+ (\/ @pathpart)* \/? ">"       { \p s -> (p, TokenURL (tail $ init $ trim s)) }

{ 
-- Each action has type :: AlexPosn -> String -> TokenPair
-- The token type: 
data Token = 
  TokenWhite              |
  TokenFrom               |
  TokenFile               |
  TokenIs                 |
  TokenOf                 |
  TokenWhere              |
  TokenOutput             |
  TokenIn                 |
  TokenInt Int            |
  TokenVar String         |
  TokenPossesive String   |
  TokenPlus               |
  TokenMinus              |
  TokenTimes              |
  TokenDiv                |
  TokenX                  |
  TokenLoad               |
  TokenBool Bool          |
  TokenIsnt               |
  TokenReplacement        |
  TokenNull               |
  TokenAny                |
  TokenComma              |
  TokenDot                |
  TokenEnd                |
  TokenObject             |
  TokenSubject            |
  TokenPredicate          |
  TokenLessThan           |
  TokenGreaterThan        |
  TokenLBracket           |
  TokenRBracket           |
  TokenAnd                |
  TokenApplication        |
  TokenTo                 |
  TokenFileName String    |
  TokenString String      |
  TokenURL String
  deriving (Eq,Show) 

type TokenPair = (AlexPosn, Token)

tokenPosn :: TokenPair -> String
tokenPosn ((AlexPn _ l c), _) = (show l) ++ ":" ++ (show c)

trim :: String -> String
trim = dropWhileEnd isSpace

}
