{
module Grammar where
import Tokens
} 

%name parseSTQL
%tokentype { TokenPair }
%error { parseError } 

%token
white                     { (_, TokenWhite) }
from                      { (_, TokenFrom) }
file                      { (_, TokenFile) }
is                        { (_, TokenIs) }
where                     { (_, TokenWhere) }
output                    { (_, TokenOutput) }
in                        { (_, TokenIn) }
of                        { (_, TokenOf) }
int                       { (_, TokenInt $$) }
var                       { (_, TokenVar $$) }
possessive                { (_, TokenPossesive $$) }
'+'                       { (_, TokenPlus) }
'-'                       { (_, TokenMinus) }
'*'                       { (_, TokenTimes) }
'/'                       { (_, TokenDiv) }
'x'                       { (_, TokenX) }
load                      { (_, TokenLoad) }
bool                      { (_, TokenBool $$) }
isnt                      { (_, TokenIsnt) }
replacement               { (_, TokenReplacement) }
null                      { (_, TokenNull) }
any                       { (_, TokenAny) }
','                       { (_, TokenComma) }
'.'                       { (_, TokenDot) }
';'                       { (_, TokenEnd) }
object                    { (_, TokenObject) }
subject                   { (_, TokenSubject) }
predicate                 { (_, TokenPredicate) }
'<'                       { (_, TokenLessThan) }
'>'                       { (_, TokenGreaterThan) }
'{'                       { (_, TokenLBracket) }
'}'                       { (_, TokenRBracket) }
and                       { (_, TokenAnd) }
application               { (_, TokenApplication) }
to                        { (_, TokenTo) }
fileName                  { (_, TokenFileName $$) }
string                    { (_, TokenString $$) }
url                       { (_, TokenURL $$) }

%%
Program : InFlightProgram output var {$1 ++ [Output $3]}

InFlightProgram : {[]}
                | InFlightProgram Load          {$1 ++ [$2]}
                | InFlightProgram Select        {$1 ++ [$2]}
                | InFlightProgram Const         {$1 ++ [$2]}
                | InFlightProgram Application   {$1 ++ [$2]}
                | InFlightProgram Replace       {$1 ++ [$2]}


Load : load var from file fileName {Load $2 $5}
Application : var is application of Math to var {Application $1 $5 $7}
Math : Math Math '+' {Plus $1 $2}
     | Math Math '-' {Minus $1 $2}
     | Math Math '*' {Times $1 $2}
     | Math Math '/' {Divide $1 $2}
     | int           {Grammar.Int $1}
     | 'x'           {Grammar.Var}

Vars : var { [$1] }
     | Vars ',' var { $3 : $1 }

Select : var is Vars {Select $1 $3 []}
       | var is Vars where Filters {Select $1 $3 $5}

Filter : object Internal '<' int {IntegerComparison $2 LessThan $4}
       | object Internal '>' int {IntegerComparison $2 GreaterThan $4}
       | object Internal in possessive Value {SetComparison [Object] $2 $4 [$5]}
       | subject Internal in possessive Value {SetComparison [Subject] $2 $4 [$5]}
       | predicate Internal in possessive Value {SetComparison [Predicate] $2 $4 [$5]}
       | Values Internal in possessive Values {SetComparison $1 $2 $4 $5}

Filters : Filter { [$1] }
        | Filters and Filter { $3 : $1 }

Value : object { Object }
      | subject { Subject }
      | predicate { Predicate }

InFlightValues : '{' {[]}
       | InFlightValues Value {$2 : $1}

Values : InFlightValues '}' {$1}

Internal : is { Is }
         | isnt { Isnt }
         
Const : var is '{' TripleSequence '.' '}' {Const $1 $4}
      | var is null                   {Const $1 []}
      
TripleSequence : EXTriple { $1 }
               | TripleSequence '.' EXTriple { $3 ++ $1 }

Replace: var is replacement of Triple in var {Replace $1 $5 $7}

Triple :: {Triple}
Triple : Mid Mid End {Triple $1 $2 $3}

EXTriple :: { [Triple] }
EXTriple : url EXMids {landTriple (MidUrl $1) $2}
       | any EXMids {landTriple MidAny $2}

EXMid :: { (Mid,[End]) }
EXMid : Mid EXEnds {($1, $2)}

EXMids :: { [(Mid,[End])] }
EXMids : EXMid {[$1]}
     | EXMids ';' EXMid {$3 : $1}

End :: { End }
End : url {EndUrl $1}
    | any {EndAny}
    | string {EndString $1}
    | int {EndInt $1}
    | bool {EndBool $1}

Mid :: { Mid }
Mid : url {MidUrl $1}
    | any {MidAny}

EXEnds :: { [End] }
EXEnds : End {[$1]}
     | EXEnds ',' End {$3 : $1}

{ 
parseError :: [TokenPair] -> a
parseError c = error ((foldl (\a b -> a ++ "Parse error at " ++ tokenPosn b ++ "\n") "Errors: \n" c))
type Variable = String
type FileName = String
-- for most of program, output is Nothing
-- when output is found, output is set
-- after this point, nothing else can be added to statements
type InFlightProgram = [Statement]
type Program = [Statement]
data Statement = 
      Load Variable FileName 
    | Select Variable [Variable] [Filter]
    -- "[VAR] is NULL" is signified by empty list of triples
    | Const Variable [Triple] 
    | Application Variable Math Variable
    | Replace Variable Triple Variable
    | Output Variable
    deriving Show

data Triple = Triple Mid Mid End deriving (Eq, Ord)
data Mid = MidUrl String | MidAny deriving (Eq, Ord)
data End = EndUrl String | EndAny | EndInt Int | EndBool Bool | EndString String deriving (Eq, Ord) 

landTriple :: Mid -> [(Mid,[End])] -> [Triple]
landTriple a b = concatMap (\(c, d) -> map (\y -> Triple a c y) d) b

instance (Show Triple) where
    show (Triple a b c) = 
        --show a ++ ( ' ' : (show b ++ (' ' : (show c ++ " ."))))
        show a ++ ( (show b ++ ((show c ++ " ."))))

instance (Show Mid) where
    show (MidUrl s) = "<" ++ s ++ ">"
    show _ = "ANY"

instance (Show End) where
    show (EndAny) = 
        "ANY"
    show (EndUrl s) =
        '<' : s ++ ">"
    show (EndString s) =
        '\"' : s ++ "\""
    show (EndInt i) = 
        ' ' : (show i)
    show (EndBool True) = 
        " true"
    show (EndBool False) = 
        " false"

data Math = 
      Plus Math Math
    | Minus Math Math
    | Times Math Math
    | Divide Math Math
    | Int Int
    | Var
    deriving Show  

data Filter = 
      SetComparison [Value] Internal Variable [Value]
    | IntegerComparison Internal Direction Int
    deriving Show

data Value = Object | Subject | Predicate deriving Show
data Internal = Is | Isnt deriving Show
data Direction = LessThan | GreaterThan deriving Show
}