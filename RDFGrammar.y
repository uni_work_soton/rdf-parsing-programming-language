{
module RDFGrammar where
import RDFTokens
import Grammar (Triple(..), Mid(..), End(..))
import Data.List
} 

%name parseRDF
%tokentype { TokenPair }
%error { parseError } 

%token
base        { (_, TokenBase) }
prefix      { (_, TokenPrefix) }
'.'         { (_, TokenFullStop) }
','         { (_, TokenComma) }
';'         { (_, TokenSemicolon) }
prefixName  { (_, TokenPrefixName $$) }
uri         { (_, TokenFullUri $$) }
ufURI       { (_, TokenUnFullUri $$) }
rIRI        { (_, TokenRelativeIri $$) }
string      { (_, TokenStringLiteral $$) }
pIRI        { (_, TokenPrefixIri $$) }
int         { (_, TokenIntegerLiteral $$) }
bool        { (_, TokenBool $$) }

%%
Graph :: { Graph }
Graph : InFlightGraph {case $1 of (_, _, a) -> a} --nub $ sort $ 

InFlightGraph :: { InFlightGraph }
InFlightGraph : {("", [], [])}
              | InFlightGraph base URI '.' {case $1 of (a, b, c) -> ($3, b, c)}
              | InFlightGraph prefix prefixName Prefix '.' {case $1 of (a, b, c) -> (a, ($3, $4) : (filter (\(y,_) -> y /= $3)  b), c)}
              | InFlightGraph InFlightTriple '.' {case $1 of (a, b, c) -> (a, b, (landTriple a b $2) ++ c)}

InFlightTriple :: { InFlightTriple }
InFlightTriple : URI Mids {(EXMidUrl $1, $2)}
               | rIRI Mids {(EXMidRIri $1, $2)}
               | pIRI Mids {(EXMidPIri $1, $2)}

Mid :: { (EXMid,[EXEnd]) }
Mid : URI Ends {(EXMidUrl $1, $2)}
    | rIRI Ends {(EXMidRIri $1, $2)}
    | pIRI Ends {(EXMidPIri $1, $2)}

Mids :: { [(EXMid,[EXEnd])] }
Mids : Mid {[$1]}
     | Mids ';' Mid {$3 : $1}

End :: { EXEnd }
End : URI {EXEndUrl $1}
    | rIRI {EXEndRIri $1}
    | pIRI {EXEndPIri $1}
    | string {EXEndString $1}
    | int {EXEndInt $1}
    | bool {EXEndBool $1}

Ends :: { [EXEnd] }
Ends : End {[$1]}
     | Ends ',' End {$3 : $1}

Prefix :: { Prefix }
Prefix : rIRI {P_RIRI $1}
       | URI  {P_URI $1}

URI :: { URI }
URI : uri {$1}
    | ufURI {"http://" ++ $1}

{ 
parseError :: [TokenPair] -> a
parseError c = error ((foldl (\a b -> a ++ "Parse error at " ++ tokenPosn b ++ "\n") "Errors: \n" c))

type Base = String
type Prefixes = [(String, Prefix)]
type URI = String
data Prefix = P_RIRI String | P_URI URI
type InFlightGraph = (Base, Prefixes, [Triple])
type Graph = [Triple]

type InFlightTriple = (EXMid, [(EXMid,[EXEnd])])
data EXMid = EXMidUrl String | EXMidPIri [String] | EXMidRIri String deriving Show
data EXEnd = EXEndUrl String | EXEndPIri [String] | EXEndRIri String | EXEndString String | EXEndInt Int | EXEndBool Bool deriving Show

landTriple :: Base -> Prefixes -> InFlightTriple -> [Triple]
landTriple base prefixes (a,b) = let aa = expandMid base prefixes a in concatMap (\(c, d) -> let cc = expandMid base prefixes c in map (\y -> Triple aa cc (expandEnd base prefixes y)) d) b

expandMid :: Base -> Prefixes -> EXMid -> Mid
expandMid _ _ (EXMidUrl u) = MidUrl u
expandMid b p (EXMidPIri u) = MidUrl (expandPIRI b p u)
expandMid b _ (EXMidRIri u) = MidUrl (b ++ u)

expandEnd :: Base -> Prefixes -> EXEnd -> End
expandEnd _ _ (EXEndUrl u) = EndUrl u
expandEnd b p (EXEndPIri u) = EndUrl (expandPIRI b p u)
expandEnd b _ (EXEndRIri u) = EndUrl (b ++ u)
expandEnd _ _ (EXEndString u) = EndString u
expandEnd _ _ (EXEndInt u) = EndInt u
expandEnd _ _ (EXEndBool u) = EndBool u

expandPIRI :: Base -> Prefixes -> [String] -> URI
expandPIRI _ _ [] = ""
expandPIRI _ _ [u] = u
expandPIRI b p (u:us) = (case find (\a -> fst a == u) p of Just (_, P_URI a) -> a; Just (_, P_RIRI a) -> (b ++ a); Nothing -> error ("Prefix \"" ++ u ++ "\" is not defined")) ++ (expandPIRI b p us)
}