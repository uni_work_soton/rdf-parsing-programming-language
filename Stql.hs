{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE LambdaCase #-}

import Control.Monad
import Data.Char
import Data.List
import Data.Maybe
import Grammar
import RDFGrammar
import RDFTokens
import System.Environment
import System.IO
import Tokens

type Variables = [(Variable, Graph)]

main :: IO ()
main = do
  (input : _) <- getArgs
  contents <- readFile input
  let stqlTokens = Tokens.alexScanTokens contents
  let program = parseSTQL stqlTokens

  error_check <- foldl (flip variableMutabilityErrorChecking) (return []) (zip program [1 ..])
  let !force_error_check = hash error_check
  unused_check <- variableUnusedWarningChecking (foldl (flip variableUnusedWarningCheckingLoop) (return ([],[])) (zip program [1 ..]))
  let !force_unused_check = hash unused_check
  extra_checks <- foldl (\x y -> do xx <- x; yy <- y; return (show yy ++ xx)) (return "") (concat (zipWith (curry (\x -> map (\y -> y x) checks)) program [1 ..]))
  let !force_extra_check = hash extra_checks
  
  foldl (flip stqlRouter) (return []) program
  return ()

stqlRouter :: Statement -> IO Variables -> IO Variables
stqlRouter (Load var filename) = stqlLoad var filename
stqlRouter (Select var vars filters) = stqlSelect var vars filters
stqlRouter (Const var triples) = stqlConst var triples
stqlRouter (Application var1 math var2) = stqlApplication var1 math var2
stqlRouter (Replace var1 triple var2) = stqlReplace var1 triple var2
stqlRouter (Output var) = stqlOutput var

stqlLoad :: Variable -> FileName -> IO Variables -> IO Variables
stqlLoad v fname vars = do
  contents <- readFile fname
  vars' <- vars
  let out = (v, parseRDF (RDFTokens.alexScanTokens contents)) : vars'
  return out

stqlConst :: Variable -> Graph -> IO Variables -> IO Variables
stqlConst varName triples vars = do vars' <- vars; return ((varName, triples) : vars')

stqlReplace :: Variable -> Triple -> Variable -> IO Variables -> IO Variables
stqlReplace out (Triple ina inb inc) input vars = do vars' <- vars; return ((out, map (\(Triple a b c) -> Triple (if ina == MidAny then a else ina) (if inb == MidAny then b else inb) (if inc == EndAny then c else inc)) (getVar vars' input)) : vars')

stqlApplication :: Variable -> Math -> Variable -> IO Variables -> IO Variables
stqlApplication out math input vars = do vars' <- vars; return (let lambda = makeMathLambda math in (out, map (\(Triple a b c) -> Triple a b (case c of (EndInt d) -> EndInt (lambda d); d -> d)) (getVar vars' input)) : vars')

makeMathLambda :: Math -> (Int -> Int)
makeMathLambda (Plus a b) x = makeMathLambda a x + makeMathLambda b x
makeMathLambda (Minus a b) x = makeMathLambda a x - makeMathLambda b x
makeMathLambda (Times a b) x = makeMathLambda a x * makeMathLambda b x
makeMathLambda (Divide a b) x = makeMathLambda a x `div` makeMathLambda b x
makeMathLambda (Int a) _ = a
makeMathLambda Var x = x

stqlSelect :: Variable -> [Variable] -> [Filter] -> IO Variables -> IO Variables
stqlSelect out inputs filters vars = do vars' <- vars; return (let lambda = makeFilterLambda filters vars' in (out, filter lambda (concatMap (getVar vars') inputs)) : vars')

makeFilterLambda :: [Filter] -> Variables -> (Triple -> Bool)
makeFilterLambda [] _ _ = True
makeFilterLambda ((SetComparison value1 internal variable value2) : fs) vars triple = (case internal of Is -> elem; Isnt -> notElem) (tripleCompile value1 triple) (map (tripleCompile value2) (getVar vars variable)) && makeFilterLambda fs vars triple
makeFilterLambda ((IntegerComparison internal direction int) : fs) vars triple@(Triple _ _ (EndInt c)) = (case internal of Is -> id; Isnt -> not) ((case direction of LessThan -> (<); GreaterThan -> (>)) c int) && makeFilterLambda fs vars triple
makeFilterLambda _ _ _ = False

newtype CPTriple = CPTriple [End] deriving (Show)

instance (Eq CPTriple) where
  (==) (CPTriple a) (CPTriple b) = all (uncurry equal) (zip a b)

equal :: End -> End -> Bool
equal EndAny _ = True
equal _ EndAny = True
equal a b = a == b

tripleCompile :: [Value] -> Triple -> CPTriple
tripleCompile [] _ = CPTriple []
tripleCompile (Object : vs) triple@(Triple _ _ c) = CPTriple (c : case tripleCompile vs triple of CPTriple t -> t)
tripleCompile (Subject : vs) triple@(Triple a _ _) = CPTriple ((case a of MidUrl c -> EndUrl c; MidAny -> EndAny) : case tripleCompile vs triple of CPTriple t -> t)
tripleCompile (Predicate : vs) triple@(Triple _ b _) = CPTriple ((case b of MidUrl c -> EndUrl c; MidAny -> EndAny) : case tripleCompile vs triple of CPTriple t -> t)

stqlOutput :: Variable -> IO Variables -> IO Variables
stqlOutput t v = do
  v' <- v
  foldr (\y x -> do xx <- x; yy <- y; return (show yy ++ xx)) (return "") (if "debug__" `elem` map fst v' then map (\x -> print (fst x) >> stqlOutput' (nub $ sort (snd x))) v' else [stqlOutput' (nub $ sort (getVar v' t))])
  v

stqlOutput' :: [Triple] -> IO ()
stqlOutput' [] = return ()
stqlOutput' (t : ts) = do
  print t
  stqlOutput' ts

getVar :: Variables -> Variable -> Graph
getVar v s = snd $ fromJust (find (\x -> fst x == s) v)

hash :: Show a => a -> Int
hash x = sum (map ord (show x))

variableMutabilityErrorChecking :: (Statement, Int) -> IO [Variable] -> IO [Variable]
variableMutabilityErrorChecking (s, i) iovs = do
  vs <- iovs
  let (output, input) = getSides s
  let force_1 = map (\x -> if x `elem` vs then error ("ERROR Statement " ++ show i ++ ": variable " ++ x ++ " already exists") else x) output
  let !force_2 = hash (map (\x -> if x `notElem` vs then error ("ERROR Statement " ++ show i ++ ": variable " ++ x ++ " does not exist") else x) input)
  return (force_1 ++ vs)

variableUnusedWarningCheckingLoop :: (Statement, Int) -> IO ([Variable],[Variable]) -> IO ([Variable],[Variable])
variableUnusedWarningCheckingLoop (s, i) iovs = do
  (vs,used) <- iovs
  let (output, input) = getSides s
  return (output ++ vs, input ++ used)

variableUnusedWarningChecking :: IO ([Variable],[Variable]) -> IO ([Variable],[Variable])
variableUnusedWarningChecking iovs = do
  (vs,used) <- iovs
  let unused = nub (sort vs) \\ nub (sort used)
  if not (null unused) then hPutStrLn stderr ("WARNING variables are unused: " ++ show unused) >> iovs else iovs

getSides :: Statement -> ([Variable], [Variable])
getSides (Output v) = ([], [v])
getSides (Const v _) = ([v], [])
getSides (Load v _) = ([v], [])
getSides (Application o _ i) = ([o], [i])
getSides (Replace o _ i) = ([o], [i])
getSides (Select o is fs) = ([o], is ++ mapMaybe (\case SetComparison _ _ i _ -> Just i; _ -> Nothing) fs)

checks :: [(Statement, Int) -> IO (Statement, Int)]
checks = [selectBalanceErrorChecking, replaceNopWarningChecking, applicationNopWarningChecking, applicationVarIgnoredWarningChecking, selectNullWarningChecking, selectNopWarningChecking]

selectBalanceErrorChecking :: (Statement, Int) -> IO (Statement, Int)
selectBalanceErrorChecking (Select a b fs, i) = return (Select a b (map (\case SetComparison v1 _ _ v2 | length v1 /= length v2 -> error ("ERROR Statement " ++ show i ++ ": length of positions in " ++ show v1 ++ " does not match length of positions in " ++ show v2); a -> a) fs), i)
selectBalanceErrorChecking a = return a

replaceNopWarningChecking :: (Statement, Int) -> IO (Statement, Int)
replaceNopWarningChecking a@(Replace _ (Triple MidAny MidAny EndAny) _, i) = hPutStrLn stderr ("WARNING Statement " ++ show i ++ ": Replace of ANY ANY ANY performs no operation.") >> return a
replaceNopWarningChecking a = return a

applicationNopWarningChecking :: (Statement, Int) -> IO (Statement, Int)
applicationNopWarningChecking a@(Application _ Var _, i) = hPutStrLn stderr ("WARNING Statement " ++ show i ++ ": Application of identity function performs no operation.") >> return a
applicationNopWarningChecking a = return a

applicationVarIgnoredWarningChecking :: (Statement, Int) -> IO (Statement, Int)
applicationVarIgnoredWarningChecking a@(Application _ m _, i) | not (hasVar m) = hPutStrLn stderr ("WARNING Statement " ++ show i ++ ": Application of function which ignores input is equivalent to replacement.") >> return a
applicationVarIgnoredWarningChecking a = return a

hasVar :: Math -> Bool
hasVar (Plus a b) = hasVar a || hasVar b
hasVar (Minus a b) = hasVar a || hasVar b
hasVar (Times a b) = hasVar a || hasVar b
hasVar (Divide a b) = hasVar a || hasVar b
hasVar (Int _) = False
hasVar Var = True

selectNullWarningChecking :: (Statement, Int) -> IO (Statement, Int)
selectNullWarningChecking a@(Select _ [] _, i) = hPutStrLn stderr ("WARNING Statement " ++ show i ++ ": Selection with no inputs is always NULL.") >> return a
selectNullWarningChecking a = return a

selectNopWarningChecking :: (Statement, Int) -> IO (Statement, Int)
selectNopWarningChecking a@(Select _ [b] [], i) = hPutStrLn stderr ("WARNING Statement " ++ show i ++ ": Selection with one input and no filters performs no operation.") >> return a
selectNopWarningChecking a = return a
