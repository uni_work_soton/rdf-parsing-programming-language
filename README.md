A programming language made in Haskell as a group project while at university. Has many features, such as

- Ability to query and parse RDF documents
- Set union, difference and intersection of graphs
- Easy to read and write syntax
- Syntax highlighting in VSCode
- Descriptive warnings

..And much more! Read the coursework report for more details.

This was made as a part of a university module, Programming Language Concepts, in a group of three.
