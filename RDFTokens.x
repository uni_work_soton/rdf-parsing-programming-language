{ 
module RDFTokens where 
import Data.List
import Data.Char (isSpace)
}

%wrapper "posn" 
$digit = 0-9     
-- digits 
$alpha = [a-zA-Z]    
-- alphabetic characters

@http = "http://" | "https://"
@hostpart = [$alpha $digit \_ \-]+
@pathpart = [$alpha $digit \- \_ \~ \: \/ \? \# \[ \] \@ \! \$ \& \' \( \) \* \+ \, \; \= \.]+
@unFullUri = @hostpart (\. @hostpart)+ (\/ @pathpart)* \/?
@fullUri = @http @unFullUri

--problems to solve:
--1. how to handle the case of a URI with no forward slash at the end, need to redefine the above?
--2. add haskell data structure in post amble for all tokens



tokens :-
  
  $white+ ;
  "@base"         {\p s -> (p, TokenBase) }
  "@prefix" {\p s -> (p, TokenPrefix) }
  "<" @fullUri ">" {\p s -> (p, TokenFullUri (tail $ init $ trim s)) }
  "<" @unFullUri ">" {\p s -> (p, TokenUnFullUri (tail $ init $ trim s)) }

  "true"                                    { \p s -> (p, (TokenBool True)) }
  "false"                                   { \p s -> (p, (TokenBool False)) }
  "."         {\p s -> (p, TokenFullStop) }
  ","         {\p s -> (p, TokenComma) }
  ";"         {\p s -> (p, TokenSemicolon) }

  [\+\-]?[0-9]+ {\p s -> (p, TokenIntegerLiteral (read $ (dropWhile (== '+') s))) }
  \" $alpha* \" {\p s -> (p, TokenStringLiteral (tail $ init $ trim s)) }
  
  $alpha* (":" [$alpha $digit]+)+ {\p s -> let x = map (dropWhile (== ':')) (groupBy (\x y -> x == ':') s) in (p, TokenPrefixIri x)}
  $alpha* ":" {\p s -> (p, TokenPrefixName (init $ trim s)) }
  "<" @pathpart (\/ @pathpart)* \/? ">" {\p s -> (p, TokenRelativeIri (tail $ init $ trim s)) }
{

data Token = 
  TokenBase         |
  TokenPrefix         |
  TokenFullStop       |
  TokenComma          |
  TokenSemicolon      |
  TokenPrefixName String    |
  TokenFullUri String       |
  TokenUnFullUri String     |
  TokenRelativeIri String     |
  TokenStringLiteral String |
  TokenPrefixIri [String] |
  TokenIntegerLiteral Int |
  TokenBool Bool
  deriving (Eq,Show) 
tokenPosn :: TokenPair -> String
tokenPosn ((AlexPn _ l c), _) = (show l) ++ ":" ++ (show c)
type TokenPair = (AlexPosn, Token)

trim :: String -> String
trim = dropWhileEnd isSpace
}