{ 
module Maze where 
}

%wrapper "posn" 
$digit = 0-9     
-- digits 
$alpha = [a-zA-Z]    
-- alphabetic characters

tokenpairs :-
  $white+       ; 
  "--".*        ; 
  if            { \p s -> (p,TokenIf) }
  else          { \p s -> (p,TokenElse) }
  f " " $digit+     { \p s -> (p,TokenForward (read (tail s))) }
  o " " 1-9         { \p s -> (p,TokenObstacle (read (tail s))) }
  l             { \p s -> (p,TokenLeft) }
  r             { \p s -> (p,TokenRight) }
  \{            { \p s -> (p,TokenLBracket) }
  \}            { \p s -> (p,TokenRBracket) }

{ 
-- Each action has type :: AlexPosn -> String -> TokenPair
-- The token type: 
data Token = 
  TokenIf |
  TokenElse |
  TokenForward Int |
  TokenObstacle Int |
  TokenLeft |
  TokenRight |
  TokenLBracket |
  TokenRBracket
  deriving (Eq,Show) 

type TokenPair = (AlexPosn, Token)

tokenPosn :: TokenPair -> String
tokenPosn ((AlexPn _ l c), _) = (show l) ++ ":" ++ (show c)

}
