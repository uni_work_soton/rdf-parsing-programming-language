{ 
module MGrammar where 
import Maze 
}

%name parseCalc 
%tokentype { TokenPair } 
%error { parseError }
%token 
    "if" { (_, TokenIf) } 
    "else"  { (_, TokenElse) } 
    'f' { (_, TokenForward $$) } 
    'o' { (_, TokenObstacle $$) } 
    'l' { (_, TokenLeft) } 
    'r' { (_, TokenRight) } 
    '{' { (_, TokenLBracket) } 
    '}' { (_, TokenRBracket) } 

%right if else
%%
Block : {- empty -}         { [] }
      | Block Exp           { $1 ++ [$2] }
Exp : 'f'                   { Forward $1 }
    | 'l'                   { MGrammar.Left } 
    | 'r'                   { MGrammar.Right } 
    | "if" 'o' '{' Block '}' "else" '{' Block '}'           { If $2 $4 $8 } 
    
{ 
parseError :: [TokenPair] -> a
parseError c = error ((foldl (\a b -> a ++ "Parse error at " ++ tokenPosn b ++ "\n") "Errors: \n" c))
type Block = [Exp]
data Exp = Forward Int
         | Left
         | Right
         | If Int Block Block
         deriving Show 
} 