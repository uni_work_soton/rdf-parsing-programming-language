{ 
module Grammar where 
import Tokens 
}

%name parseCalc 
%tokentype { TokenPair } 
%error { parseError }
%token 
    let { (_, TokenLet) } 
    in  { (_, TokenIn) } 
    int { (_, TokenInt $$) } 
    var { (_, TokenVar $$) } 
    '=' { (_, TokenEq) } 
    '+' { (_, TokenPlus) } 
    '-' { (_, TokenMinus) } 
    '*' { (_, TokenTimes) } 
    '/' { (_, TokenDiv) } 
    '(' { (_, TokenLParen) } 
    ')' { (_, TokenRParen) }
    '^' { (_, TokenExp) }

%right in 
%left '+' '-' 
%left '*' '/' 
%left '^'
%left NEG 
%% 
Exp : let var '=' Exp in Exp { Let $2 $4 $6 } 
    | Exp '+' Exp            { Plus $1 $3 } 
    | Exp '-' Exp            { Minus $1 $3 } 
    | Exp '*' Exp            { Times $1 $3 } 
    | Exp '/' Exp            { Div $1 $3 }
    | Exp '^' Exp            { Expn $1 $3 }
    | '(' Exp ')'            { $2 } 
    | '-' Exp %prec NEG      { Negate $2 } 
    | int                    { Int $1 } 
    | var                    { Var $1 } 
    
{ 
parseError :: [TokenPair] -> a
parseError c = error ((foldl (\a b -> a ++ "Parse error at " ++ tokenPosn b ++ "\n") "Errors: \n" c))
data Exp = Let String Exp Exp 
         | Plus Exp Exp 
         | Minus Exp Exp 
         | Times Exp Exp 
         | Div Exp Exp 
         | Expn Exp Exp 
         | Negate Exp
         | Int Int 
         | Var String 
         deriving Show 
} 