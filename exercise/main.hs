import MGrammar
import Maze
import System.Environment

main :: IO ()
main = do
  (input : _) <- getArgs
  contents <- readFile input
  let tokens = alexScanTokens contents
  let exp = parseCalc tokens
  print exp