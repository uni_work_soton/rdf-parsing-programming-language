{ 
module Tokens where 
}

%wrapper "posn" 
$digit = 0-9     
-- digits 
$alpha = [a-zA-Z]    
-- alphabetic characters

tokenpairs :-
$white+       ; 
  "--".*        ; 
  let           { \p s -> (p,TokenLet) }
  in            { \p s -> (p,TokenIn) }
  $digit+       { \p s -> (p,TokenInt (read s)) }
  \=          { \p s -> (p,TokenEq) }
  \^          { \p s -> (p,TokenExp) }
  \+          { \p s -> (p,TokenPlus) }
  \-          { \p s -> (p,TokenMinus) }
  \*          { \p s -> (p,TokenTimes) }
  \/          { \p s -> (p,TokenDiv) }
  \(          { \p s -> (p,TokenLParen) }
  \)          { \p s -> (p,TokenRParen) }
  $alpha [$alpha $digit \_ \’]*   { \p s -> (p,TokenVar s) }

{ 
-- Each action has type :: AlexPosn -> String -> TokenPair
-- The token type: 
data Token = 
  TokenLet         | 
  TokenIn          | 
  TokenInt Int     |
  TokenVar String  | 
  TokenEq          |
  TokenExp         |
  TokenPlus        |
  TokenMinus       |
  TokenTimes       |
  TokenDiv         |
  TokenLParen      |
  TokenRParen       
  deriving (Eq,Show) 

type TokenPair = (AlexPosn, Token)

tokenPosn :: TokenPair -> String
tokenPosn ((AlexPn _ l c), _) = (show l) ++ ":" ++ (show c)

}
